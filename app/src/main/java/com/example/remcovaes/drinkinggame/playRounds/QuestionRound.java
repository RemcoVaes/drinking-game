package com.example.remcovaes.drinkinggame.playRounds;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.example.remcovaes.drinkinggame.GetQuestionOffline;
import com.example.remcovaes.drinkinggame.QuestionRequestHandlerSingleton;
import com.example.remcovaes.drinkinggame.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Random;

/**
 * Created by remcovaes on 04-Mar-17.
 */
public class QuestionRound extends AppCompatActivity {
    private GetQuestionOffline handler;
    private String correctAnswer;
    private JSONArray wrongAnswers;
    ArrayList<String> options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question);
        Response.Listener<String> request = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject question = (JSONObject)new JSONObject(response).getJSONArray("results").opt(0);
                    correctAnswer = Jsoup.parse(question.getString("correct_answer")).text();
                    wrongAnswers = question.getJSONArray("incorrect_answers");
                    ((TextView) findViewById(R.id.question))
                            .setText(Jsoup.parse(question.getString("question")).text());
                    options = new ArrayList<>();
                    options.add(correctAnswer);
                    for (int i = 0; i < wrongAnswers.length();i++) {
                        options.add(Jsoup.parse(wrongAnswers.optString(i)).text());
                    }

                    Collections.shuffle(options);
                    Button one = (Button)findViewById(R.id.optionOne);
                    Button two = (Button)findViewById(R.id.optionTwo);
                    Button three = (Button)findViewById(R.id.optionThree);
                    Button four = (Button)findViewById(R.id.optionFour);
                    one.setText(options.get(0));
                    two.setText(options.get(1));
                    three.setText(options.get(2));
                    four.setText(options.get(3));
                    one.setEnabled(true);
                    two.setEnabled(true);
                    three.setEnabled(true);
                    four.setEnabled(true);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        QuestionRequestHandlerSingleton rq = QuestionRequestHandlerSingleton.getHandler();
        rq.getQuestion(this,request,this.getIntent().getStringExtra("difficulty"));

    }

    public void optionOne (View view) {
        checkScreen(checkAnswer(0),view);
    }

    public void optionTwo (View view) {
        checkScreen(checkAnswer(1),view);
    }

    public void optionThree (View view) {
        checkScreen(checkAnswer(2),view);
    }

    public void optionFour (View view) {
        checkScreen(checkAnswer(3),view);
    }


    public Boolean checkAnswer(int i) {
        return this.options.get(i).equals(this.correctAnswer);
    }

    public void checkScreen(Boolean correctAnswer, View view) {
        Intent intent = new Intent(view.getContext(), QuestionAnswerScreen.class);
        intent.putExtra("difficulty",getIntent().getStringExtra("difficulty"));
        intent.putExtra("correct",correctAnswer);
        intent.putExtra("correctAnswer",this.correctAnswer);
        startActivityForResult(intent, 0);
    }


}
