package com.example.remcovaes.drinkinggame.playRounds;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.R;
import com.example.remcovaes.drinkinggame.SingletonData;

/**
 * Created by remcovaes on 04-Mar-17.
 */
public class QuestionPlayerScreen extends AppCompatActivity {
    private SingletonData data;

    //todo: put this screen in the minigame start screen

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_player_screen);
        data = SingletonData.getData();
        String player = data.getPlayers().get((data.getTurn()) % data.getPlayers().size());
        ((TextView) findViewById(R.id.playerName)).setText(player);
    }
    public void goToQuestion(View view) {
        Intent intent = new Intent(view.getContext(), SelectDifficulty.class);
        startActivityForResult(intent, 0);
    }
}
