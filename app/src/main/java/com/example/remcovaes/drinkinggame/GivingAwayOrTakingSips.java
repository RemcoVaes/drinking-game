package com.example.remcovaes.drinkinggame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.miniGameController.MinigameStartScreen;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by remcovaes on 05-Feb-17.
 */
public class GivingAwayOrTakingSips extends AppCompatActivity{
    ArrayList<String> listOfPlayers;
    Random random;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.give_away_or_taking_sips);

        random = new Random();

        int sips = getIntent().getIntExtra("numberOfSips",
                random.nextBoolean() ? random.nextInt(5) + 1: -random.nextInt(5) -1);

        listOfPlayers = getIntent().getStringArrayListExtra("playerNames");

        String printText = listOfPlayers.get(getIntent()
                .getIntExtra("totalPlayerTurns",0)%listOfPlayers.size());
        if (sips > 0) {
            printText = printText + ", you can give away ";
        }
        else {
            printText = printText + ", you have to take ";
        }
        ((TextView) findViewById(R.id.amountOfDrinks))
                .setText(printText + (random.nextInt(5) + 1) + " sip(s)");
    }

    public void nextPlayer(View view) {
        Intent intent = new Intent(view.getContext(), MinigameStartScreen.class);
        startActivityForResult(intent, 0);
    }


}
