package com.example.remcovaes.drinkinggame.playRounds;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.R;
import com.example.remcovaes.drinkinggame.ScoreOverview;
import com.example.remcovaes.drinkinggame.SingletonData;
import com.example.remcovaes.drinkinggame.miniGameController.MinigameStartScreen;

/**
 * Created by remcovaes on 04-Mar-17.
 */
public class QuestionAnswerScreen extends AppCompatActivity{
    SingletonData data = SingletonData.getData();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_answer_screen);
        ((TextView)findViewById(R.id.answer)).setText(getIntent().getStringExtra("correctAnswer"));
        if (data.getTurn()%data.getPlayers().size() == data.getKing()) {
            if (getIntent().getBooleanExtra("correct",false)) {
                if(getIntent().getStringExtra("difficulty").equals("hard")) {
                    ((TextView)findViewById(R.id.drink)).setText("you are King, give out 6 sips");
                } else if (getIntent().getStringExtra("difficulty").equals("medium")) {
                    ((TextView)findViewById(R.id.drink)).setText("you are King, give out 4 sips");
                } else {
                    ((TextView)findViewById(R.id.drink)).setText("you are King, give out 3 sips");
                }

            } else {
                ((TextView)findViewById(R.id.drink)).setText("you are King, Kings don't have to drink");
            }
        } else {
            if (getIntent().getBooleanExtra("correct",false)) {
                if (getIntent().getStringExtra("difficulty").equals("hard")) {
                    ((TextView)findViewById(R.id.drink)).setText("you can give out 5 sips");
                } else if (getIntent().getStringExtra("difficulty").equals("medium")) {
                    ((TextView)findViewById(R.id.drink)).setText("you can give out 3 sips");
                } else {
                    ((TextView)findViewById(R.id.drink)).setText("you can give out 2 sips");
                }
            } else {

                if (getIntent().getStringExtra("difficulty").equals("hard")) {
                    ((TextView)findViewById(R.id.drink)).setText("you have to drink 4 sips");
                } else if (getIntent().getStringExtra("difficulty").equals("medium")) {
                    ((TextView)findViewById(R.id.drink)).setText("you have to drink 3 sips");
                } else {
                    ((TextView)findViewById(R.id.drink)).setText("you have to drink 2 sips");
                }
            }
        }
    }
    public void goToNextPlayer(View view) {
        data.incrementTurn();
        if((data.getTurn()%data.getPlayers().size()) == 0) {
            data.changeCurrentGame();
            Intent intent = new Intent(view.getContext(), MinigameStartScreen.class);
            startActivityForResult(intent, 0);
            return;
        }
        Intent intent = new Intent(view.getContext(), SelectDifficulty.class);
        startActivityForResult(intent, 0);

    }


}
