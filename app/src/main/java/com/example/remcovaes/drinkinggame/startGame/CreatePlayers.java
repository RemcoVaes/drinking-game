package com.example.remcovaes.drinkinggame.startGame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.remcovaes.drinkinggame.R;

import java.util.ArrayList;


/**
 * Created by remcovaes on 03-Feb-17.
 */
public class CreatePlayers extends AppCompatActivity {
    private int totalPlayers;
    private int playersMade;
    private ArrayList<String> playerNames;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_players);
        totalPlayers = getIntent().getIntExtra("NUMBER_OF_PLAYERS",0);
        playersMade = getIntent().getIntExtra("TOTAL_PLAYERS_MADE", 0);
        playerNames = getIntent().getStringArrayListExtra("playerNames");
    }

    public void startPlaying(View view) {
        Intent myIntent = new Intent(view.getContext(), CreatePlayers.class);
        playerNames.add(((EditText) findViewById(R.id.playerName)).getText().toString());
        myIntent.putExtra("playerNames",playerNames);
        if (playersMade +1 == totalPlayers) {
            Intent intent = new Intent(view.getContext(), PlayersCheck.class);
            intent.putExtra("playerNames", playerNames);
            startActivityForResult(intent, 0);
            return;
        }
        myIntent.putExtra("NUMBER_OF_PLAYERS", this.totalPlayers);
        myIntent.putExtra("TOTAL_PLAYERS_MADE",this.playersMade+1);
        startActivityForResult(myIntent, 0);
    }
}
