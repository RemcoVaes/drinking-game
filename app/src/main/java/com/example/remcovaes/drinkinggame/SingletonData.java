package com.example.remcovaes.drinkinggame;

import com.example.remcovaes.drinkinggame.miniGames.CoinCollector;
import com.example.remcovaes.drinkinggame.miniGames.FiveSecondsClick;
import com.example.remcovaes.drinkinggame.playRounds.QuestionRound;
import com.example.remcovaes.drinkinggame.playRounds.SelectDifficulty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.lang.Math.abs;

/**
 * Created by remcovaes on 13-Feb-17.
 */
public class SingletonData {
    static SingletonData obj = new SingletonData();
    private ArrayList<Class> miniGames;
    private Class currentGame;
    private ArrayList<String> players = new ArrayList<>();
    private Integer turn = 0;
    private LinkedHashMap<Integer,Integer> score = new LinkedHashMap<>();
    private Random random = new Random();
    private int king;

    public LinkedHashMap<Integer, Integer> getScore() {
        return score;
    }

    private SingletonData() {
        miniGames = new ArrayList<>();
        miniGames.add(FiveSecondsClick.class);
        miniGames.add(CoinCollector.class);

        changeCurrentGame();
    }
    public void changeCurrentGame(){
        currentGame = miniGames.get(random.nextInt(miniGames.size()));
    }

    public void setCurrentGame(Class currentGame) {
        this.currentGame = currentGame;
    }

    public Class getCurrentGame() {
        return currentGame;
    }

    public static SingletonData getData() {
        return obj;
    }

    public ArrayList<String> getPlayers() {
        return players;
    }

    public void sortScores() {
        List list = new LinkedList(score.entrySet());
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return abs(((int)((Map.Entry) (o1)).getValue())) - (abs((int)((Map.Entry) (o2)).getValue()));
            }
        });
        LinkedHashMap<Integer,Integer> sortedHashMap = new LinkedHashMap<>();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put((int)entry.getKey(), (int)entry.getValue());
        }
        this.score = sortedHashMap;
    }

    public void setPlayers(ArrayList<String> players) {
        this.players = players;
    }

    public Integer getTurn() {
        return turn;
    }

    public int getKing() {
        return king;
    }

    public void setKing(int king) {
        this.king = king;
    }

    public void incrementTurn() {
        if (turn == 1 ){
            miniGames.add(SelectDifficulty.class);
            miniGames.add(SelectDifficulty.class);
            miniGames.add(SelectDifficulty.class);
        }
        this.turn++;
    }
}
