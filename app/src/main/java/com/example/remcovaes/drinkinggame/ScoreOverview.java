package com.example.remcovaes.drinkinggame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.miniGameController.MinigameStartScreen;
import com.example.remcovaes.drinkinggame.playRounds.QuestionPlayerScreen;
import com.example.remcovaes.drinkinggame.playRounds.QuestionRound;

/**
 * Created by remcovaes on 13-Feb-17.
 */
public class ScoreOverview extends AppCompatActivity {
    SingletonData data = SingletonData.getData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coin_collector_scores);
        data.sortScores();
        int rank = 1;
        int king = data.getScore().entrySet().iterator().next().getKey();
        data.setKing(king);

        ((TextView) findViewById(R.id.theDrinkKing))
                .setText(data.getPlayers().get(king));
        for (Integer i:data.getScore().keySet()) {
            TextView temp = new TextView(this);
            temp.setText(rank + ": " + data.getPlayers().get(i) + " Time was: " + data.getScore().get(i));
            temp.setTextSize(24);

            ((LinearLayout) findViewById(R.id.coinCollectorScores)).addView(temp);
            rank++;
        }
        data.changeCurrentGame();
    }
    public void playNextRound(View view) {
        Intent intent = new Intent(view.getContext(), QuestionPlayerScreen.class);
        startActivityForResult(intent, 0);
    }
}
