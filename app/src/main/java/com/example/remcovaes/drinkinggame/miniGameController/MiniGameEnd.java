package com.example.remcovaes.drinkinggame.miniGameController;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.R;
import com.example.remcovaes.drinkinggame.ScoreOverview;
import com.example.remcovaes.drinkinggame.SingletonData;

/**
 * Created by remcovaes on 08-Feb-17.
 */
public class MiniGameEnd extends AppCompatActivity{
    SingletonData data = SingletonData.getData();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score_overview);

        Long l = (getIntent().getLongExtra("time",10));
        Integer i = l.intValue();
        ((TextView) findViewById(R.id.timeCoinCollector)).setText(i.toString());

        data.getScore().put(data.getTurn() % data.getPlayers().size(), i);

    }

    public void goToNextPlayer(View view) {
        data.incrementTurn();
        if((data.getTurn()%data.getPlayers().size()) == 0) {
            Intent intent = new Intent(view.getContext(), ScoreOverview.class);
            startActivityForResult(intent, 0);
            return;
        }
        Intent intent = new Intent(view.getContext(), MinigameStartScreen.class);
        startActivityForResult(intent, 0);

    }
}
