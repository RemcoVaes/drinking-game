package com.example.remcovaes.drinkinggame.miniGameController;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.R;
import com.example.remcovaes.drinkinggame.SingletonData;
import com.example.remcovaes.drinkinggame.miniGames.CoinCollector;
import com.example.remcovaes.drinkinggame.miniGames.FiveSecondsClick;
import com.example.remcovaes.drinkinggame.playRounds.QuestionPlayerScreen;

/**
 * Created by remcovaes on 05-Feb-17.
 */
public class MinigameStartScreen extends AppCompatActivity{
    private SingletonData data;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        data = SingletonData.getData();


        if(data.getCurrentGame().equals(QuestionPlayerScreen.class)) {
            Intent intent = new Intent(findViewById(android.R.id.content).getContext(), data.getCurrentGame());
            startActivityForResult(intent, 0);
            return;
        }
        setContentView(R.layout.start_minigame);
        String player = data.getPlayers().get((data.getTurn()) % data.getPlayers().size());
        ((TextView) findViewById(R.id.playerName)).setText(player);

        if (data.getCurrentGame().equals(CoinCollector.class)) {
            ((TextView) findViewById(R.id.gameExplanation)).setText("Collect 10 coins as fast as possible!");
        } else if (data.getCurrentGame().equals(FiveSecondsClick.class)) {
            ((TextView) findViewById(R.id.gameExplanation)).setText("Press the button on the next screen to start, then press it again after five seconds!");
        }

    }

    public void startMinigame(View view) {
        Intent intent = new Intent(view.getContext(), data.getCurrentGame());
        startActivityForResult(intent, 0);
    }


}
