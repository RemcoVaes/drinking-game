package com.example.remcovaes.drinkinggame;

import android.content.Context;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by remcovaes on 04-Mar-17.
 */
public class QuestionRequestHandlerSingleton {
    static QuestionRequestHandlerSingleton obj = getObject();
    private String token;

    private QuestionRequestHandlerSingleton() throws Exception {
    }

    private static QuestionRequestHandlerSingleton getObject() {
        try {
            return new QuestionRequestHandlerSingleton();
        } catch (Exception e) {
            return null;
        }
    }

    public static QuestionRequestHandlerSingleton getHandler() {
        return obj;
    }

    public void getQuestion(Context ct, Response.Listener<String> resp,String difficulty) {
        sendRequest(ct,resp,
                "https://www.opentdb.com/api.php?amount=1&type=multiple&difficulty="
                        +difficulty+"&token="+this.token);
    }

    public void getToken(Context ct, Response.Listener<String> resp) {
        sendRequest(ct,resp,"https://www.opentdb.com/api_token.php?command=request");
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void sendRequest(Context ct,Response.Listener<String> resp,String link) {
        RequestQueue queue = Volley.newRequestQueue(ct);
        String url = link;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                resp, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}


