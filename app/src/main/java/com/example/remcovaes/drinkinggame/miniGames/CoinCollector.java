package com.example.remcovaes.drinkinggame.miniGames;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.miniGameController.MiniGameEnd;
import com.example.remcovaes.drinkinggame.R;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by remcovaes on 08-Feb-17.
 */
public class CoinCollector extends AppCompatActivity{
    private Integer coinTotal = 0;
    private TextView counter;
    private Calendar startTime;
    private Button coin;
    private Random random;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coin_collector);
        counter = (TextView)findViewById(R.id.coinsCounter);
        startTime = Calendar.getInstance();
        coin = (Button) findViewById(R.id.coin);
        random = new Random();
    }

    public void coinIsClicked(View view) {
        //todo : add a start game button before the first coin is displayed
        //todo : hide the coin if the last coin is clicked
        coinTotal++;
        counter.setText(coinTotal.toString());
        if (coinTotal == 10) {
            Calendar endTime = Calendar.getInstance();
            Intent intent = new Intent(view.getContext(), MiniGameEnd.class);
            intent.putExtra("time",endTime.getTimeInMillis()-startTime.getTimeInMillis());
            startActivityForResult(intent, 0);
        }
        coin.setX(random.nextInt(800));
        coin.setY(random.nextInt(1000));
    }
}
