package com.example.remcovaes.drinkinggame.miniGames;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.R;
import com.example.remcovaes.drinkinggame.miniGameController.MiniGameEnd;

import java.util.Calendar;

import static java.lang.Math.abs;

/**
 * Created by Remco Vaes on 24/02/2017.
 */

public class FiveSecondsClick extends AppCompatActivity {
    private boolean firstClickHappened;
    private Calendar startTime;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firstClickHappened = false;
        setContentView(R.layout.five_seconds_click);
    }

    public void buttonTimerIsClicked(View view) {
        if (firstClickHappened) {
            Calendar endTime = Calendar.getInstance();
            Intent intent = new Intent(view.getContext(), MiniGameEnd.class);
            intent.putExtra("time",(endTime.getTimeInMillis()-startTime.getTimeInMillis())-5000);
            startActivityForResult(intent, 0);
        } else {
            startTime = Calendar.getInstance();
            ((TextView) findViewById(R.id.timerStart)).setVisibility(View.VISIBLE);
            firstClickHappened = true;
        }
    }
}
