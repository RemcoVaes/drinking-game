package com.example.remcovaes.drinkinggame.startGame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.remcovaes.drinkinggame.QuestionRequestHandlerSingleton;
import com.example.remcovaes.drinkinggame.R;
import com.example.remcovaes.drinkinggame.SingletonData;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.ReferenceQueue;

public class MainActivity extends AppCompatActivity {
    QuestionRequestHandlerSingleton requestHandler = QuestionRequestHandlerSingleton.getHandler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.start_screen);
        Response.Listener<String> request = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    requestHandler.setToken((new JSONObject(response)).getString("token"));
                    System.out.println((new JSONObject(response)).getString("token"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        requestHandler.getToken(this,request);

    }

    public void goToNumberOfPlayers(View view) {
        Intent myIntent = new Intent(view.getContext(), NumberOfPlayers.class);
        startActivityForResult(myIntent,0);
    }


}