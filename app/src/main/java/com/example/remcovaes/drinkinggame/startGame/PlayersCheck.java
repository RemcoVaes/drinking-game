package com.example.remcovaes.drinkinggame.startGame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.R;
import com.example.remcovaes.drinkinggame.SingletonData;
import com.example.remcovaes.drinkinggame.miniGameController.MinigameStartScreen;
import com.example.remcovaes.drinkinggame.playRounds.QuestionRound;

import java.util.ArrayList;

/**
 * Created by remcovaes on 03-Feb-17.
 */
public class PlayersCheck extends AppCompatActivity{
    private ArrayList<TextView> players;
    private ArrayList<String> playerNames;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.players_check);

        players = new ArrayList<>();
        players.add((TextView)findViewById(R.id.player1));
        players.add((TextView)findViewById(R.id.player2));
        players.add((TextView)findViewById(R.id.player3));
        players.add((TextView)findViewById(R.id.player4));
        players.add((TextView)findViewById(R.id.player5));
        players.add((TextView)findViewById(R.id.player6));
        players.add((TextView)findViewById(R.id.player7));
        players.add((TextView)findViewById(R.id.player8));

        playerNames = getIntent().getStringArrayListExtra("playerNames");
        for (int i = 0;i < playerNames.size();i++) {
            players.get(i).setText(playerNames.get(i));
        }

    }

    public void goToMinigameStart(View view) {
        Intent intent = new Intent(view.getContext(), MinigameStartScreen.class);
        SingletonData data = SingletonData.getData();
        data.setPlayers(playerNames);
        startActivityForResult(intent, 0);
    }
}
