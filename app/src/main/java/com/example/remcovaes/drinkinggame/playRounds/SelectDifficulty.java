package com.example.remcovaes.drinkinggame.playRounds;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.remcovaes.drinkinggame.R;
import com.example.remcovaes.drinkinggame.SingletonData;

/**
 * Created by remcovaes on 09-Mar-17.
 */
public class SelectDifficulty extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_choose_difficulty);

    }

    public void easySelected(View view) {
        goToQuestion(view,"easy");
    }
    public void mediumSelected(View view) {
        goToQuestion(view,"medium");
    }
    public void hardSelected(View view) {
        goToQuestion(view,"hard");
    }
    private void goToQuestion(View view, String difficulty){
        Intent intent = new Intent(view.getContext(), QuestionRound.class);
        intent.putExtra("difficulty",difficulty);
        startActivityForResult(intent, 0);
    }
}
