package com.example.remcovaes.drinkinggame.startGame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.example.remcovaes.drinkinggame.R;
import com.example.remcovaes.drinkinggame.startGame.CreatePlayers;

import java.util.ArrayList;

public class NumberOfPlayers extends AppCompatActivity {
    Spinner numberOfPlayersSpinner;
    Button playTheGameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.number_of_players);

        numberOfPlayersSpinner = (Spinner) findViewById(R.id.numberOfPlayerSpinner);
        playTheGameButton = (Button) findViewById(R.id.playTheGameButton);



    }

    public void startTheGame(View view) {
        Intent myIntent = new Intent(view.getContext(), CreatePlayers.class);
        myIntent.putExtra("NUMBER_OF_PLAYERS", Integer.parseInt(numberOfPlayersSpinner.getSelectedItem().toString()));
        myIntent.putExtra("TOTAL_PLAYERS_MADE", 0);
        myIntent.putExtra("playerNames", new ArrayList<String>());
        startActivityForResult(myIntent, 0);
    }

}
